import React, { Component } from 'react'
import './global.css'
import 'antd/dist/antd.css'

// Import react router
import { BrowserRouter, Route } from 'react-router-dom'

// Import redux libraries
import { createStore, applyMiddleware } from 'redux'
import { Provider, connect } from 'react-redux'
import thunkMiddleware from 'redux-thunk'

// Import app reducers
import reducers from '~/reducers'
import { requestTokenVerification } from '~/actionCreators/user'

// Import the app viewport
import Viewport from '~/components/Viewport.js'

// Import helper libraries
import { injectProps } from '~/libraries/injectProps'

// Import components 
import LoadingOverlay from '~/components/LoadingOverlay'

// Import screens
import HomeScreen from '~/screens/HomeScreen'
import AboutScreen from '~/screens/AboutScreen'
import ProfileScreen from '~/screens/ProfileScreen'

// Create the redux store 
export const store = createStore(
    reducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(
        thunkMiddleware
    )
)

/**
 * Main Application root component
 */
class App extends Component {

    constructor(props) {
        super(props)
        this.state = {
            loading: false
        }
    }

    componentDidMount = () => {

        // Check for a stored authentication token
        //const authToken = localStorage.getItem(`${process.env.REACT_APP_CODE_NAME}_AUTH_TOKEN`) || window.providedAuthKey
        const authToken = 'Cf9g0B4GL8ZdPru21btqRyDSaL2aOShfH8vO7qPnieCVdQVBzfm0SmbNcqGoId22'
        if (authToken && !store.user) {
            this.setState({ loading: true })
            this.props.requestTokenVerification(authToken).then(() => {
                this.setState({ loading: false })
            })
        } else {
            this.setState({ loading: false })
        }
    }

    wrapInsideViewport = ScreenComponent => props =>
        <Viewport>
            {<ScreenComponent
                {...props}
            />}
        </Viewport>

    render () {
        return (
            <Provider store={store}>
                <BrowserRouter>
                    {this.state.loading ? <LoadingOverlay size='large' backgroundColor='rgba(255, 255, 255, 1)' /> :
                        <div>
                            {/* Wrap these screens inside the app viewport */}
                            <Route exact path="/" component={this.wrapInsideViewport(HomeScreen)} />
                        </div>
                    }
                </BrowserRouter>
            </Provider>
        );
    }
}

// Connect the redux reducer result
const mapDispatchToProps = dispatch => {
    return {
        requestTokenVerification: (token) => dispatch(requestTokenVerification(token))
    }
}

export default injectProps(connect(null, mapDispatchToProps)(App), { store })