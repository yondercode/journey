import ActionTypes from '~/ActionTypes'

/**
 * Request the list of all destinations
 * @param {string} token 
 */
export function requestHotels () {
    return dispatch => fetch(process.env.REACT_APP_API_ENDPOINT + '/PartnerHotels/', {
        method: 'GET'
    })
        .then(response => response.json(), error => console.log(error))
        .then(json => dispatch(receiveHotels(json)))
}

/**
 * Receive the list of all destinations
 * @param {array} destinations 
 */
export function receiveHotels (hotels) {
    return {
        type: ActionTypes.ReceiveHotels,
        error: hotels ? false : true,
        payload: hotels
    }
}