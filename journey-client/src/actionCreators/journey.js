import ActionTypes from '~/ActionTypes'

/**
 * Request all journeys owned by an user
 * @param {string} token 
 */
export function requestUserJourneys (token, id) {
    return dispatch => fetch(process.env.REACT_APP_API_ENDPOINT + '/Journeys/GetForUser/' + id, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        },
    })
        .then(response => response.json(), error => console.log(error))
        .then(json => dispatch(receiveUserJourney(json)))
}

/**
 * Receive the journeys of an suer
 * @param {string} token 
 */
export function receiveUserJourney (data) {
    return {
        type: ActionTypes.ReceiveUserJourneys,
        error: data ? false : true,
        payload: data
    }
}

/**
 * Create a new journey
 * @param {array} token 
 * @param {array} destinationId 
 * @param {array} hotelPackageId 
 * @param {array} sourceLat 
 * @param {array} sourceLong 
 * @param {array} transportationType 
 */
export function postJourney (token, userid, destinationId, hotelPackageId, sourceLat, sourceLong) {
    return dispatch => fetch(process.env.REACT_APP_API_ENDPOINT + '/Journeys/', {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify({
            UserId: userid,
            PartnerDestinationId: destinationId,
            PartnerHotelPackageId: hotelPackageId,
            SourceLat: sourceLat,
            SourceLong: sourceLong,
        })
    })
}

/**
 * Receive the response of creating a journey
 * @param {object} data 
 */
export function receivePostJourneyResponse (data) {
    return {
        type: ActionTypes.ReceivePostJourneyResponse,
        error: data ? false : true,
        payload: data
    }
}

// /**
//  * Create a new journey
//  * @param {array} token 
//  * @param {array} destinationId 
//  * @param {array} hotelPackageId 
//  * @param {array} sourceLat 
//  * @param {array} sourceLong 
//  * @param {array} transportationType 
//  */
// export function updateJourney (token, journeyId, destinationId, hotelPackageId, sourceLat, sourceLong, transportationType) {
//     return dispatch => fetch(process.env.REACT_APP_API_ENDPOINT + '/Journeys/' + journeyId, {
//         method: 'PUT',
//         headers: {
//             'Authorization': `Bearer ${token}`,
//             'Content-Type': 'json'
//         },
//         body: {
//             PartnerDestinationId: destinationId,
//             PartnerHotelPackageId: hotelPackageId,
//             SourceLat: sourceLat,
//             SourceLong: sourceLong,
//             TransportationType: transportationType
//         }
//     })
//         .then(response => response.json(), error => console.log(error))
//         .then(json => dispatch(receivePostDestinationReviewResponse(json)))
// }

// /**
//  * Receive the response of updating a journey
//  * @param {object} data 
//  */
// export function receiveUpdateJourneyResponse (data) {
//     return {
//         type: ActionTypes.ReceiveUpdateJourneyResponse,
//         error: data ? false : true,
//         payload: data
//     }
// }

// /**
//  * Create a new journey
//  * @param {string} token 
//  * @param {int} journeyId 
//  * @param {string} paymentMethod 
//  */
// export function payJourney (token, journeyId, paymentMethod) {
//     return dispatch => fetch(process.env.REACT_APP_API_ENDPOINT + '/Journeys/PutPay/' + journeyId, {
//         method: 'POST',
//         headers: {
//             'Authorization': `Bearer ${token}`,
//             'Content-Type': 'json'
//         },
//         body: {
//             PaymentMethod: paymentMethod,
//         }
//     })
//         .then(response => response.json(), error => console.log(error))
//         .then(json => dispatch(receivePostDestinationReviewResponse(json)))
// }

// /**
//  * Receive a response from pay journey action
//  * @param {string} token 
//  */
// export function receivePayJourneyResponse (data) {
//     return {
//         type: ActionTypes.ReceivePayJourneyResponse,
//         error: data ? false : true,
//         payload: data
//     }
// }

// /**
//  * Deletes a journey
//  * @param {string} token 
//  * @param {int} journeyId 
//  */
// export function deleteJourney (token, journeyId) {
//     return dispatch => fetch(process.env.REACT_APP_API_ENDPOINT + '/Journeys/' + journeyId, {
//         method: 'DELETE',
//         headers: {
//             'Authorization': `Bearer ${token}`
//         }
//     })
//         .then(response => response.json(), error => console.log(error))
//         .then(json => dispatch(receivePostDestinationReviewResponse(json)))
// }

// /**
//  * Receive a response from delete journey action
//  * @param {string} token 
//  */
// export function receiveDeleteJourneyResponse (data) {
//     return {
//         type: ActionTypes.ReceiveDeleteJourneyResponse,
//         error: data ? false : true,
//         payload: data
//     }
// }