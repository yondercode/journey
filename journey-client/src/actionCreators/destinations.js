import ActionTypes from '~/ActionTypes'

/**
 * Request the list of all destinations
 * @param {string} token 
 */
export function requestDestinations () {
    return dispatch => fetch(process.env.REACT_APP_API_ENDPOINT + '/PartnerDestinations/', {
        method: 'GET'
    })
        .then(response => response.json(), error => console.log(error))
        .then(json => dispatch(receiveDestinations(json)))
}

/**
 * Receive the list of all destinations
 * @param {array} destinations 
 */
export function receiveDestinations (destinations) {
    return {
        type: ActionTypes.ReceiveDestinations,
        error: destinations ? false : true,
        payload: destinations
    }
}

/**
 * Request the detail of a destination, including
 * @param {string} token 
 * @param {string} id 
 */
export function requestDestinationDetails (token, id) {
    return dispatch => fetch(process.env.REACT_APP_API_ENDPOINT + '/PartnerDestinations/' + id, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        },
    })
        .then(response => response.json(), error => console.log(error))
        .then(json => dispatch(receiveDestinationDetails(json)))
}

/**
 * Receive one detail of a destination
 * @param {object} data 
 */
export function receiveDestinationDetails (data) {
    return {
        type: ActionTypes.ReceiveDestinationDetails,
        error: data ? false : true,
        payload: data
    }
}

/**
 * Post a review of a destination
 * @param {string} token 
 * @param {string} destinationId
 * @param {float} rating
 * @param {string} content
 */
export function postDestinationReview (token, destinationId, rating, content, journeyId) {
    return dispatch => fetch(process.env.REACT_APP_API_ENDPOINT + '/PartnerDestinationReview/', {
        method: 'POST',
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'json'
        },
        body: {
            PartnerDestinationId: destinationId,
            Content: content,
            Rating: rating,
            JourneyId: journeyId
        }
    })
        .then(response => response.json(), error => console.log(error))
        .then(json => dispatch(receivePostDestinationReviewResponse(json)))
}

/**
 * Receive the response of creating a review
 * @param {object} data 
 */
export function receivePostDestinationReviewResponse (data) {
    return {
        type: ActionTypes.ReceivePostDestinationReviewResponse,
        error: data ? false : true,
        payload: data
    }
}