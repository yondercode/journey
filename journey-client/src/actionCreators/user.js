import ActionTypes from '~/ActionTypes'

/**
 * Attempts a token verification request.
 * @param {string} token 
 */
export function requestTokenVerification (token) {
    return dispatch => fetch(process.env.REACT_APP_API_ENDPOINT + '/Users/GetValidateAuthToken/' + token, {
        method: 'GET',
    }).then(response => response.json(), error => console.log(error))
        .then(json => dispatch(receiveAuthResponse(token, json)))
}

/**
 * Add the received token and user data to the store
 * @param {string} token 
 * @param {any} user 
 */
export function receiveAuthResponse (token, user, remember) {
    if (token) {
        console.log(user)
        if (remember) localStorage.setItem("authToken", token)
    }
    return {
        type: ActionTypes.ReceiveAuthResponse,
        error: token ? false : true,
        payload: token ? {
            token,
            user
        } : {}
    }
}

/**
 * Logs out from the application
 * @param {string} token 
 */
export function attemptLogout (token) {
    return dispatch => fetch(process.env.REACT_APP_API_ENDPOINT + '/logout', {
        method: 'POST',
        headers: {
            'content-type': 'application/json'
        },
        body: JSON.stringify({ token: token })
    }).then(response => response.json(), error => console.log(error))
        .then(json => dispatch(receiveLogoutResponse(json)))
}

/**
 * Receive the logout response
 * @param {any} response 
 */
export function receiveLogoutResponse (response) {
    localStorage.setItem("authToken", "")
    return {
        type: ActionTypes.ReceiveLogoutResponse,
        error: response !== true
    }
}

/**
 * Updates an user data
 * @param {string} token 
 * @param {string} displayName 
 * @param {string} password 
 */
export function updateUser (token, userId, displayName, password) {
    return dispatch => fetch(process.env.REACT_APP_API_ENDPOINT + '/Users/' + userId, {
        method: 'PUT',
        headers: {
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'json'
        },
        body: {
            DisplayName: displayName,
            Password: password,
        }
    })
        .then(response => response.json(), error => console.log(error))
        .then(json => dispatch(receiveUpdateUserResponse(json)))
}

/**
 * Receive the response of updating a journey
 * @param {any} data 
 */
export function receiveUpdateUserResponse (data) {
    return {
        type: ActionTypes.ReceiveUpdateUserResponse,
        error: data ? false : true,
        payload: data
    }
}

/**
 * Request the detail of an user
 * @param {string} token 
 */
export function requestUserDetails (token, id) {
    return dispatch => fetch(process.env.REACT_APP_API_ENDPOINT + '/Users/' + id, {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${token}`
        },
    })
        .then(response => response.json(), error => console.log(error))
        .then(json => dispatch(receiveUserDetails(json)))
}

/**
 * Receive the user details
 * @param {any} data 
 */
export function receiveUserDetails (data) {
    return {
        type: ActionTypes.ReceiveUserDetails,
        error: data ? false : true,
        payload: data
    }
}