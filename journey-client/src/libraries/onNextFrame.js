export default callback => window.setTimeout(() => {
    window.requestAnimationFrame(callback)
}, 0)