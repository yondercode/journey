import React from 'react'

export function injectProps(Component, injectedProps) {
	return props => <Component {...{ ...injectedProps, ...props }} />
}