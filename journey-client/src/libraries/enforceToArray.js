export function enforceToArray(x) {
	if (x == null) return []
	if (x.constructor === Array) return x
	return [x]
}