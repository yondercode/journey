export default (ax, ay, bx, by) => {
    let dx = bx - ax
    let dy = by - ay
    return Math.sqrt(dx * dx + dy * dy);
}