export default function () {
    let resultClassname = ''

    for (let i = 0; i < arguments.length; i++) {
        if (arguments[i]) resultClassname = resultClassname.concat(resultClassname ? ' ' + arguments[i] : arguments[i])
    }
    
    return resultClassname
}