/**
 * List of all action types in the Redux architecture
 */

export default {
	TestAction: "TEST_ACTION",

    // Auth & user actions
    ReceiveAuthResponse: "RECEIVE_AUTH_RESPONSE",
    UpdateUser: "UPDATE_USER",
    ReceiveUpdateUserResponse: "RECEIVE_UPDATE_USER_RESPONSE",
    RequestUserDetails: "REQUEST_USER_DETAILS",
    ReceiveUserDetails: "RECEIVE_USER_DETAILS",
    
    // Destionation actions
	RequestDestinations: "REQUEST_DESTINATIONS",
    ReceiveDestinations: "RECEIVE_DESTINATIONS",
    RequestDestinationDetails: "REQUEST_DESTINATION_DETAILS",
    ReceiveDestinationDetails: "RECEIVE_DESTINATION_DETAILS",
    PostDestinationReview: "POST_DESTINATION_REVIEW",
    ReceivePostDestinationReviewResponse: "RECEIVE_POST_DESTINATION_REVIEW_RESPONSE",
    
    // Hotel & package actions
    ReceiveHotels: "RECEIVE_HOTELS",
    PostHotelReview: "POST_DESTINATION_REVIEW",
    ReceivePostHotelReviewResponse: "RECEIVE_POST_DESTINATION_REVIEW_RESPONSE",
    
    // Journey actions
    RequestUserJourneys: "REQUEST_USER_JOURNEYS",
    ReceiveUserJourneys: "RECEIVE_USER_JOURNEYS",
    PostJourney: "POST_JOURNEY",
    ReceivePostJourneyResponse: "RECEIVE_POST_JOURNEY_RESPONSE",
    UpdateJourney: "UPDATE_JOURNEY",
    ReceiveUpdateJourneyResponse: "RECEIVE_UPDATE_JOURNEY_RESPONSE",
    PayJourney: "PAY_JOURNEY",
    ReceivePayJourneyResponse: "RECEIVE_PAY_JOURNEY_RESPONSE",
    DeleteJourney: "DELETE_JOURNEY",
    ReceiveDeleteJourneyResponse: "RECEIVE_DELETE_JOURNEY_RESPONSE",
}