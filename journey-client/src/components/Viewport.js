import React from 'react'
import styles from './Viewport.css'
import combineStyles from '~/libraries/combineStyles'
import { connect } from 'react-redux'
import { Row, Col, Icon, Input, Button, Avatar, Menu, Dropdown, Modal, message, Spin } from 'antd'
import { attemptLogin, attemptLogout } from '~/actionCreators/user'
import { Link } from "react-router-dom";

class Viewport extends React.Component {
    constructor(props) {
        console.log("again lol")
        super(props)
        this.state = {
            userLoading: false
        }
    }

    handleLogout = () => {
        this.setState({ userLoading: true })
        this.props.attemptLogout(this.props.authToken).then(() => {
            message.success('See you!')
            this.setState({ userLoading: false })
        })
    }

    render () {

        // The currently logged in user
        const user = this.props.user

        const userDropdownMenu = (
            <Menu>
                <Menu.Item key="0">
                    <Icon type="user" style={{ marginRight: "6px" }} /> <span>Profile</span>
                </Menu.Item>
                <Menu.Item key="1" onClick={this.handleLogout}>
                    <Icon type="logout" style={{ marginRight: "6px" }} /> <span>Log Out</span>
                </Menu.Item>
            </Menu>
        );

        return (
            <div>

                {/* Render the content */}
                <div>
                    <Row>
                        <Col className={combineStyles(styles.contentContainer)} xs={24}>
                            {this.props.children}
                        </Col>
                    </Row>
                </div>

                {/* Render the top navigation bar */}
                <div className={combineStyles(styles.topbar)}>
                    {/* Hamburger toggle menu */}
                    {/*<div
                        className={combineStyles(styles.hamburger, (this.state.sidemenuOpen ? styles.open : null))}
                        onClick={() => this.setState({ sidemenuOpen: this.state.sidemenuOpen ? false : true })}
                    >
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                        <p>MENU</p>
                    </div>*/}

                    <Row style={{ width: "100%" }}>
                        <Col xs={24} style={{ height: "50px", padding: "0px 20px", display: "flex", alignItems: "center", justifyContent: 'space-between' }}>

                            {/* Journey Logo */}
                            <Link to='/'>
                                <img className={styles.topbarElements} src={window.location.origin + '/static/img/journey.png'} alt="CoinPi" style={{ height: "42px", marginRight: "16px", display: "inline-block", float: "left" }} />
                            </Link>

                            {/* Search box */}
                            <div className={styles.topbarElements} style={{ height: "100%" }}>
                                <Input.Search
                                    placeholder="Cari Destinasi"
                                    size="large"
                                    onSearch={value => console.log(value)}
                                    style={{ width: 400, margin: '0 12px', display: 'inline-block' }}
                                />
                            </div>

                            {/* User */}
                            {/* If there's no logged in user, show the login/register buttons */}
                            {user ? (
                                this.state.userLoading ?
                                    <Spin indicator={<Icon type="loading" />} />
                                    :
                                    <Dropdown getPopupContainer={() => document.getElementById("userdropdowncontainer")} overlay={userDropdownMenu} trigger={['click']}>
                                        <div className={combineStyles(styles.topbarElements, styles.userTab, (this.state.userTabOpen ? styles.open : null))} style={{ padding: "0px 16px" }}>
                                            <span style={{ fontSize: "18px" }}>{user.DisplayName}</span>
                                            <Avatar src={user.avatar_url ? user.avatar_url : undefined} size="medium" icon="user" style={{ marginLeft: "10px" }} />
                                            <div id="userdropdowncontainer" />
                                        </div>
                                    </Dropdown>
                            ) :
                                <div className={styles.topbarElements}>
                                    <Button onClick={() => this.setState({ openLoginModal: true })} style={{ margin: '0 6px', display: 'inline-block' }}><b>Login</b></Button>
                                    <Button type="primary" style={{ margin: '0 6px', display: 'inline-block' }}><b>Register</b></Button>
                                </div>
                            }

                        </Col>
                    </Row>

                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user,
    authToken: state.authToken
})

const mapDispatchToProps = dispatch => ({
    attemptLogout: (token) => dispatch(attemptLogout(token)),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Viewport)