import React from 'react'
import { connect } from 'react-redux'
import styles from './Sidebar.css'
import distance from '~/libraries/distance'
import SidebarItem from './SidebarItem'
import LoadingOverlay from '../LoadingOverlay'

class Sidebar extends React.Component {
    constructor(props) {
        super(props)
    }

    render () {
        const { focusDestination, focusHotel, sourceLat, sourceLong, mode, selectedDestination } = this.props
        if (mode === 'destination') {
            return (
                <div className={styles.container}>
                    <div className={styles.header}>
                        Destinasi Populer
                    </div>
                    <div className={styles.content}>
                        {!this.props.destinations ? <LoadingOverlay /> :
                            <div>
                                {this.props.destinations.map(destination => <SidebarItem
                                    key={destination.Id}
                                    name={destination.name}
                                    lat={destination.lat}
                                    long={destination.long}
                                    onClick={() => focusDestination(destination)}
                                    sourceLat={sourceLat}
                                    sourceLong={sourceLong}
                                />)}
                            </div>}
                    </div>
                </div>
            )
        }
        else {
            // Find everything that is below 10 KM
            let inRangeHotels
            if (this.props.hotels) {
                const max = 10
                inRangeHotels = this.props.hotels.filter(hotel => {
                    console.log(distance(sourceLat, sourceLong, hotel.lat, hotel.long))
                    if (distance(selectedDestination.lat, selectedDestination.long, hotel.lat, hotel.long) * 111 > max) return false
                    return true
                })
            }
            console.log(inRangeHotels)
            return (
                <div className={styles.container}>
                    <div className={styles.header}>
                        Hotel Terdekat (10KM)
                </div>
                    <div className={styles.content}>
                        {this.props.hotels ? (inRangeHotels ?
                            <div>
                                {inRangeHotels.map(hotel => <SidebarItem
                                    key={hotel.Id}
                                    name={hotel.name}
                                    lat={hotel.lat}
                                    long={hotel.long}
                                    onClick={() => focusHotel(hotel)}
                                    sourceLat={selectedDestination.lat}
                                    sourceLong={selectedDestination.long}
                                />)}
                            </div> : "Tidak ada hotel yang dekat!") : <LoadingOverlay />}
                    </div>
                </div>
            )
        }
    }
}

const mapStateToProps = state => ({
    destinations: state.destinations,
    hotels: state.hotels,
})

export default connect(
    mapStateToProps
)(Sidebar)