import React from 'react'
import { Rate, Button } from 'antd'
import { connect } from 'react-redux'
import distance from '~/libraries/distance'
import styles from './SelectedDestinationDetails.css'

class SelectedDestinationDetails extends React.Component {

    render () {
        const { sourceLat, sourceLong, selectDestination } = this.props
        const { name, description, lat, long, rating, img, PartnerDestinationReviews } = this.props.focusedDestination

        let overallRating = Math.round(rating * 2) / 2.0

        return (
            <div className={styles.container}>
                <div className={styles.header}>
                    <h1>{name}</h1>
                    <Rate allowHalf defaultValue={overallRating} />
                    <span>{parseFloat(distance(sourceLat, sourceLong, lat, long) * 111).toFixed(2)} Km</span>
                </div>    
                <div className={styles.description}>
                    <div style={{
                        backgroundImage: `url("${img ? window.location.origin + '/' + img : 'https://www.my-vb.com/img/assets/missing_image.jpg'}")`,
                        height: '220px',
                        backgroundPosition: 'center'
                    }}/>
                    <p>
                        {description}
                    </p>    
                </div>   
                <div className={styles.reviews}>
                </div>   
                
                <Button
                    size='large'
                    type='primary'
                    disabled={this.props.user ? false : true}
                    onClick={selectDestination}
                    style={{ width: '100%', marginTop: '12px' }}>
                    <b style={{ fontSize: '18px' }}>{this.props.user ? 'Pilih Destinasi' : 'Mohon Login Dahulu!'}</b>
                </Button>
                
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user,
})

export default connect(
    mapStateToProps
)(SelectedDestinationDetails)