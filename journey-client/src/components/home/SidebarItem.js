import React from 'react'
import { Rate } from 'antd'
import distance from '~/libraries/distance'
import styles from './SidebarItem.css'

class Sidebar extends React.Component {

    render () {
        const { name, lat, long, sourceLat, sourceLong, onClick } = this.props

        return (
            <div className={styles.container} onClick={onClick}>
                <h1>{name}</h1>
                <span>{parseFloat(distance(sourceLat, sourceLong, lat, long) * 111).toFixed(2)} Km</span>
                <Rate allowHalf defaultValue={4.5} />
            </div>
        )
    }
}

export default Sidebar