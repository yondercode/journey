import React from 'react'
import { Steps } from 'antd'
import { connect } from 'react-redux'
import styles from './BottomProgressBar.css'

class BottomProgressBar extends React.Component {
    constructor(props) {
        super(props)
    }

    render () {
        const { selectedDestination, selectedHotel, selectedHotelPackage, resetDestination, resetHotel } = this.props
        let current = 0
        if (selectedDestination) current = 1
        if (selectedHotelPackage) current = 2
        return (
            <div className={styles.container}>
                {
                    this.props.user ?
                        <Steps current={current}>
                            <Steps.Step title="Destinasi" description={selectedDestination ? <span>{selectedDestination.name} | <a onClick={resetDestination}> Ganti </a></span> : null} />
                            <Steps.Step title="Hotel" description={selectedHotel ? <span>{selectedHotel.name} - {selectedHotelPackage.name} | <a onClick={resetHotel}> Ganti </a></span> : null} />
                            <Steps.Step title="Penyimpanan" />
                        </Steps>
                        :
                        <span style={{ fontSize: '22px' }}>
                            <a href={window.location.origin + '/login.aspx'}>Login</a> atau <a href={window.location.origin + '/register.aspx'}>Register</a> untuk memulai rencana perjalanan anda!
                        </span>
                }
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user,
})

export default connect(
    mapStateToProps
)(BottomProgressBar)