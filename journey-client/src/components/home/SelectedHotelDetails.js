import React from 'react'
import { Rate, Button } from 'antd'
import { connect } from 'react-redux'
import distance from '~/libraries/distance'
import styles from './SelectedHotelDetails.css'

class SelectedHotelDetails extends React.Component {

    render () {
        const { sourceLat, sourceLong, selectHotel, selectedDestination } = this.props
        const { name, description, lat, long, img, PartnerHotelPackages } = this.props.focusedHotel

        let overallRating = 4.5

        return (
            <div className={styles.container}>
                <div className={styles.header}>
                    <h1>{name}</h1>
                    <Rate allowHalf defaultValue={overallRating} />
                    <span>{parseFloat(distance(selectedDestination.lat, selectedDestination.long, lat, long) * 111).toFixed(2)} Km</span>
                </div>    
                <div className={styles.description}>
                    <div style={{
                        backgroundImage: `url("${img ? window.location.origin + '/' + img : 'https://www.my-vb.com/img/assets/missing_image.jpg'}")`,
                        height: '220px',
                        backgroundPosition: 'center'
                    }}/>
                    <p>
                        {description}
                    </p>    
                </div>   
                <div className={styles.reviews}>
                </div>   
                
                <Button
                    size='large'
                    type='primary'
                    disabled={this.props.user ? false : true}
                    onClick={selectHotel}
                    style={{ width: '100%', marginTop: '12px' }}>
                    <b style={{ fontSize: '18px' }}>{this.props.user ? 'Pilih Hotel' : 'Mohon Login Dahulu!'}</b>
                </Button>
                
            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user,
})

export default connect(
    mapStateToProps
)(SelectedHotelDetails)