import React from 'react'
import { Rate, Button, Modal } from 'antd'
import { connect } from 'react-redux'
import distance from '~/libraries/distance'
import styles from './ConfirmationDetails.css'
import { postJourney } from '~/actionCreators/journey'

class ConfirmationDetails extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            loading: false
        }
    }

    handleSaveJourney = () => {

        this.props.postJourney(
            this.props.user.AuthToken,
            this.props.user.Id,
            this.props.selectedDestination.Id,
            this.props.selectedHotel.Id,
            this.props.sourceLat,
            this.props.sourceLong
        )
        Modal.success({
            title: 'Perjalanan anda berhasil disimpan!',
        });
    }

    render () {
        const { dname, ddescription, dlat, dlong } = this.props.selectedDestination
        const { hname, hdescription, hlat, hlong } = this.props.selectedHotel

        return (
            <div className={styles.container}>
                <div className={styles.header}>
                    <span>Destinasi</span>
                    <h1>{dname}</h1>
                </div>
                <div className={styles.header}>
                    <span>Hotel</span>
                    <h1>{hname}</h1>
                </div>

                <Button
                    size='large'
                    type='primary'
                    disabled={this.props.user ? false : true}
                    onClick={this.handleSaveJourney}
                    style={{ width: '100%', marginTop: '12px' }}>
                    <b style={{ fontSize: '18px' }}>{this.props.user ? 'Simpan Rencana' : 'Mohon Login Dahulu!'}</b>
                </Button>

            </div>
        )
    }
}

const mapStateToProps = state => ({
    user: state.user
})

const mapDispatchToProps = dispatch => ({
    postJourney: (token, id, destinationId, hotelPackageId, sourceLat, sourceLong) =>
        dispatch(postJourney(token, id, destinationId, hotelPackageId, sourceLat, sourceLong)),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ConfirmationDetails)