import React from 'react'
import { Spin } from 'antd'

export default ({ size = 'default', backgroundColor = "rgba(255, 255, 255, 0.7)", height = undefined, width = undefined, ...props }) => {

    let containerStyle = {
        position: 'absolute',
        display: 'inline-flex',
        left: 0,
        right: 0,
        bottom: 0,
        top: 0,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 1,
        backgroundColor: backgroundColor
    }

    if (height || width) {
        containerStyle = {
            ...containerStyle,
            display: 'flex',
            height: height ? height : '100%',
            width: width ? width : '100%',
        }
    }

    return (
        <div style={{ ...containerStyle, backgroundColor }}>
            <Spin size={size} />
        </div>
    )
}