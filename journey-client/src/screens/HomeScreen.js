import React, { Component } from 'react'
import { injectProps } from '~/libraries/injectProps'
import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
    Marker
} from "react-google-maps"
import { connect } from 'react-redux'
import { requestDestinations } from '~/actionCreators/destinations'
import { requestHotels } from '~/actionCreators/hotels'
import LoadingOverlay from '~/components/LoadingOverlay'
import BottomProgressBar from '~/components/home/BottomProgressBar'
import Sidebar from '~/components/home/Sidebar'
import SelectedDestinationDetails from '~/components/home/SelectedDestinationDetails'
import SelectedHotelDetails from '~/components/home/SelectedHotelDetails'
import ConfirmationDetails from '~/components/home/ConfirmationDetails'
import distance from '~/libraries/distance'

class HomeScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sourceLat: 0,
            sourceLong: 0,
            mapsCenterLat: 0,
            mapsCenterLong: 0,
            selectedDestination: null,
            selectedHotel: null,
            selectedHotelPackage: null,
            qty: 1
        }
    }

    updateSourcePosition = (lat, long) => {
        this.setState({
            sourceLat: lat,
            sourceLong: long,
            mapsCenterLat: lat,
            mapsCenterLong: long
        })
    }

    focusDestination = (destination) => {
        this.setState({
            focusedDestination: destination,
            mapsCenterLat: destination.lat,
            mapsCenterLong: destination.long
        })
    }

    selectDestination = (destination) => {
        this.setState({
            selectedDestination: destination,
            mapsCenterLat: destination.lat,
            mapsCenterLong: destination.long
        })
    }

    focusHotel = (hotel) => {
        this.setState({
            focusedHotel: hotel,
            mapsCenterLat: hotel.lat,
            mapsCenterLong: hotel.long
        })
    }

    selectHotelPackage = (hotel, hotelPackage, qty) => {
        this.setState({
            selectedHotel: hotel,
            selectedHotelPackage: hotelPackage,
            mapsCenterLat: hotel.lat,
            mapsCenterLong: hotel.long,
            qty
        })
    }

    resetHotel = () => {
        this.setState({
            selectedHotel: null,
            selectedHotelPackage: null,
            focusedHotel: null,
            qty: 1
        })
    }

    resetDestination = () => {
        this.setState({
            focusedHotel: null,
            selectedDestination: null,
            focusedDestination: null,
            selectedHotel: null,
            selectedHotelPackage: null,
            qty: 1
        })
    }

    componentDidMount = () => {
        // Obtain the user's location
        navigator.geolocation.getCurrentPosition(position => this.updateSourcePosition(position.coords.latitude, position.coords.longitude), this.handlePositionError)

        // Retrieve destination data from the server
        this.props.requestDestinations()
        this.props.requestHotels()
    }

    render () {
        // Find everything that is below 10 KM
        let inRangeHotels
        if (this.props.hotels && this.state.selectedDestination) {
            const max = 10
            inRangeHotels = this.props.hotels.filter(hotel => {
                if (distance(this.state.selectedDestination.lat, this.state.selectedDestination.long, hotel.lat, hotel.long) * 111 > max) return false
                return true
            })
        }
        return (
            <div>
                {/* Google Map Background */}
                <GoogleMap
                    defaultZoom={16}
                    defaultCenter={{ lat: this.state.sourceLat, lng: this.state.sourceLong }}
                    defaultOptions={defaultMapsOptions}
                    center={{ lat: this.state.mapsCenterLat, lng: this.state.mapsCenterLong }}
                >
                    <Marker position={{ lat: this.state.sourceLat, lng: this.state.sourceLong }} />

                    {this.state.selectedDestination ? (this.state.selectedHotel ? null :
                        inRangeHotels.map(hotel => <Marker position={{ lat: hotel.lat, lng: hotel.long }} onClick={() => this.focusHotel(hotel)} />)) :
                        this.props.destinations.map(destination => <Marker position={{ lat: destination.lat, lng: destination.long }} onClick={() => this.focusDestination(destination)} /> )
                    }
                </GoogleMap>

                {/* Side Browser */}
                <Sidebar
                    sourceLat={this.state.sourceLat}
                    sourceLong={this.state.sourceLong}
                    focusDestination={this.focusDestination}
                    focusHotel={this.focusHotel}
                    mode={this.state.selectedDestination ? 'hotel' : 'destination'}
                    selectedDestination={this.state.selectedDestination}
                />

                {/* Focused Destination */}
                {this.state.focusedDestination && !this.state.selectedDestination ? <SelectedDestinationDetails
                    sourceLat={this.state.sourceLat}
                    sourceLong={this.state.sourceLong}
                    focusedDestination={this.state.focusedDestination}
                    selectDestination={() => this.selectDestination(this.state.focusedDestination)}
                /> : null}

                {/* Focused Hotel */}
                {this.state.focusedHotel && !this.state.selectedHotel ? <SelectedHotelDetails
                    sourceLat={this.state.sourceLat}
                    sourceLong={this.state.sourceLong}
                    selectedDestination={this.state.selectedDestination}
                    focusedHotel={this.state.focusedHotel}
                    selectHotel={() => this.selectHotelPackage(this.state.focusedHotel, 1, 1)}
                /> : null}

                {/* Confirmation */}
                {this.state.selectedDestination && this.state.selectedHotel ? <ConfirmationDetails
                    sourceLat={this.state.sourceLat}
                    sourceLong={this.state.sourceLong}
                    selectedDestination={this.state.selectedDestination}
                    selectedHotel={this.state.selectedHotel}
                /> : null}

                {/* Bottom progress bar */}
                <BottomProgressBar
                    selectedDestination={this.state.selectedDestination}
                    selectedHotel={this.state.selectedHotel}
                    selectedHotelPackage={this.state.selectedHotelPackage}
                    resetDestination={this.resetDestination}
                    resetHotel={this.resetHotel}
                />
            </div>
        )
    }
}

const mapsStyle = [
    {
        "featureType": "administrative",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "lightness": 33
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2e5d4"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#c5dac6"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#c5c6c6"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e4d7c6"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#fbfaf7"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#acbcc9"
            }
        ]
    }
]

const defaultMapsOptions = {
    fullscreenControl: false,
    styles: mapsStyle,
    disableDefaultUI: true
}

const mapStateToProps = state => ({
    destinations: state.destinations,
    hotels: state.hotels,
})

const mapDispatchToProps = dispatch => ({
    requestDestinations: (token) => dispatch(requestDestinations(token)),
    requestHotels: (token) => dispatch(requestHotels(token)),
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(injectProps(withScriptjs(
    withGoogleMap(HomeScreen)
), {
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyDtHL_K8riZyBeuTQjf4piuekyksQI9tXQ",
        loadingElement: <LoadingOverlay />,
        containerElement: <div style={{ height: `100vh`, width: '100%' }} />,
        mapElement: <div style={{ height: `100%`, width: '100%' }} />,
    }))