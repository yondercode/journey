import ActionTypes from '~/ActionTypes'

/**
 * The list of all destinations
 * @param {*} state 
 * @param {*} action 
 */
export function hotels (state = [], action) {
    switch (action.type) {
        case ActionTypes.ReceiveHotels: {
            if (action.error) return state
            return action.payload
        }
        default: return state
    }
}

export default {
    hotels
}