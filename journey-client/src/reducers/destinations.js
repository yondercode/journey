import ActionTypes from '~/ActionTypes'

/**
 * The list of all destinations
 * @param {*} state 
 * @param {*} action 
 */
export function destinations (state = [], action) {
    switch (action.type) {
        case ActionTypes.ReceiveDestinations: {
            if (action.error) return state
            return action.payload
        }
        case ActionTypes.ReceivePostDestinationReviewResponse: {
            if (action.error) return state
            return state.map(x => x.id == action.payload.id ? action.payload : x)
        }
        default: return state
    }
}