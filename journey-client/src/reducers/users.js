import ActionTypes from '~/ActionTypes'

/**
 * The stored authentication token
 * @param {*} state 
 * @param {*} action 
 */
export function authToken(state = "", action) {
	switch (action.type) {
		case ActionTypes.ReceiveAuthResponse: {
			if (action.error) return ""
			return action.payload.token
		}
        case ActionTypes.ReceiveLogoutResponse: return action.error ? state : ""
		default: return state	
	}
}

/**
 * The user data
 * @param {*} state 
 * @param {*} action 
 */
export function user(state = null, action) {
	switch (action.type) {
		case ActionTypes.ReceiveAuthResponse: {
			if (action.error) return null
			return action.payload.user
		}
		case ActionTypes.ReceiveLogoutResponse: return action.error ? state : null
		default: return state
	}
}

export default {
    authToken,
    user
}