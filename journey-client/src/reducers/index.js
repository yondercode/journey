import { combineReducers } from 'redux'
import { destinations } from './destinations'
import users from './users'
import hotels from './hotels'

// Combine the whole reducers of the app
const reducers = combineReducers({
    destinations,
    ...hotels,
    ...users
})

export default reducers