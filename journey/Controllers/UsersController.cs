﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using Journey.Models;

namespace Journey.Controllers
{
    public class UsersController : ApiController
    {
        public UsersController()
        {
            User[] users = new User[]
            {
                new User { Id = 1, Email = "ayy", PasswordHash = "adasd", DisplayName = "lmao" },
                new User { Id = 4, Email = "vbn", PasswordHash = "vbnmn", DisplayName = "mm" }
            };
        }

        public IEnumerable<Product> GetAllProducts()
        {
            return products;
        }

        public IHttpActionResult GetProduct(int id)
        {
            var product = products.FirstOrDefault((p) => p.Id == id);
            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }
    }
}