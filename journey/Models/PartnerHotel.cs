﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Journey.Models
{
    public class PartnerHotel
    {
        public int Id { get; set; }
        public string MapsPlaceId { get; set; }
        public string Name { get; set; }
        public float Lat { get; set; }
        public float Long { get; set; }
        public string Description { get; set; }
        public string PreviewImageUrl { get; set; }
    }
}