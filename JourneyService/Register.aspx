﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="api.Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/bootstrap.min.css" rel="stylesheet">
    <link href="Content/login.css" rel="stylesheet">
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/jquery-1.10.2.min.js"></script>
</head>
<body>
    <%--<form id="form1" runat="server">
        <asp:Label ID="Label1" runat="server" Text="Email"></asp:Label>
        <br />
        <asp:TextBox ID="EmailField" runat="server"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label2" runat="server" Text="Password"></asp:Label>
        <br />
        <asp:TextBox ID="PasswordField" runat="server"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label3" runat="server" Text="Password Confirmation"></asp:Label>
        <br />
        <asp:TextBox ID="PasswordConfirmationField" runat="server"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="Label4" runat="server" Text="Display Name"></asp:Label>
        <br />
        <asp:TextBox ID="DisplayNameField" runat="server"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="ValidationErrorDisplay" runat="server" ForeColor="Red"></asp:Label>
        <br />
        <asp:Button ID="RegisterButton" runat="server" OnClick="RegisterButton_Click" Text="Register" />
    </form>--%>

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-login">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-6">
                                <a href="Login.aspx"  id="login-form-link">Login</a>
                            </div>
                            <div class="col-xs-6">
                                <a href="#" class="active" id="register-form-link">Register</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <form id="form1" runat="server" method="post" role="form" style="display: block;">
                                    <div class="form-group">
                                        <asp:TextBox ID="DisplayNameField" runat="server" class="form-control" placeholder="Username"></asp:TextBox>
                                        <%--<input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="">--%>
                                    </div>
                                    <div class="form-group">
                                        <asp:TextBox ID="EmailField" runat="server" class="form-control" placeholder="Email Address"></asp:TextBox>
                                        <%--<input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="">--%>
                                    </div>
                                    <div class="form-group">
                                        <asp:TextBox ID="PasswordField" runat="server" class="form-control" placeholder="Password" type="password"></asp:TextBox>
                                        <%--<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">--%>
                                    </div>
                                    <div class="form-group">
                                        <asp:TextBox ID="PasswordConfirmationField" runat="server" class="form-control" placeholder="Confirm Password" type="password"></asp:TextBox>
                                        <%--<input type="password" name="confirm-password" id="confirm-password" tabindex="2" class="form-control" placeholder="Confirm Password">--%>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="ValidationErrorDisplay" runat="server" ForeColor="Red"></asp:Label>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <asp:Button ID="RegisterButton" runat="server" OnClick="RegisterButton_Click" Text="Register" class="form-control btn btn-register" />
                                                <%--<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register Now">--%>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
