﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using api.Models;
using System.Web.Http.Cors;

namespace api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PartnerDestinationsController : ApiController
    {
        private DatabaseEntities1 db = new DatabaseEntities1();

        // GET: api/PartnerDestinations
        public IQueryable<PartnerDestinations> GetPartnerDestinations()
        {
            return db.PartnerDestinations;
        }

        // GET: api/PartnerDestinations/5
        [ResponseType(typeof(PartnerDestinations))]
        public IHttpActionResult GetPartnerDestinations(int id)
        {
            PartnerDestinations partnerDestinations = db.PartnerDestinations.Find(id);
            if (partnerDestinations == null)
            {
                return NotFound();
            }

            return Ok(partnerDestinations);
        }

        // PUT: api/PartnerDestinations/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPartnerDestinations(int id, PartnerDestinations partnerDestinations)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != partnerDestinations.Id)
            {
                return BadRequest();
            }

            db.Entry(partnerDestinations).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PartnerDestinationsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PartnerDestinations
        [ResponseType(typeof(PartnerDestinations))]
        public IHttpActionResult PostPartnerDestinations(PartnerDestinations partnerDestinations)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PartnerDestinations.Add(partnerDestinations);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (PartnerDestinationsExists(partnerDestinations.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = partnerDestinations.Id }, partnerDestinations);
        }

        // DELETE: api/PartnerDestinations/5
        [ResponseType(typeof(PartnerDestinations))]
        public IHttpActionResult DeletePartnerDestinations(int id)
        {
            PartnerDestinations partnerDestinations = db.PartnerDestinations.Find(id);
            if (partnerDestinations == null)
            {
                return NotFound();
            }

            db.PartnerDestinations.Remove(partnerDestinations);
            db.SaveChanges();

            return Ok(partnerDestinations);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PartnerDestinationsExists(int id)
        {
            return db.PartnerDestinations.Count(e => e.Id == id) > 0;
        }
    }
}