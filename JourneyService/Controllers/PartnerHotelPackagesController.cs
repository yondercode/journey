﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using api.Models;

namespace api.Controllers
{
    public class PartnerHotelPackagesController : ApiController
    {
        private DatabaseEntities1 db = new DatabaseEntities1();

        // GET: api/PartnerHotelPackages
        public IQueryable<PartnerHotelPackages> GetPartnerHotelPackages()
        {
            return db.PartnerHotelPackages;
        }

        // GET: api/PartnerHotelPackages/5
        [ResponseType(typeof(PartnerHotelPackages))]
        public async Task<IHttpActionResult> GetPartnerHotelPackages(int id)
        {
            PartnerHotelPackages partnerHotelPackages = await db.PartnerHotelPackages.FindAsync(id);
            if (partnerHotelPackages == null)
            {
                return NotFound();
            }

            return Ok(partnerHotelPackages);
        }

        // PUT: api/PartnerHotelPackages/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPartnerHotelPackages(int id, PartnerHotelPackages partnerHotelPackages)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != partnerHotelPackages.Id)
            {
                return BadRequest();
            }

            db.Entry(partnerHotelPackages).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PartnerHotelPackagesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PartnerHotelPackages
        [ResponseType(typeof(PartnerHotelPackages))]
        public async Task<IHttpActionResult> PostPartnerHotelPackages(PartnerHotelPackages partnerHotelPackages)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PartnerHotelPackages.Add(partnerHotelPackages);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PartnerHotelPackagesExists(partnerHotelPackages.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = partnerHotelPackages.Id }, partnerHotelPackages);
        }

        // DELETE: api/PartnerHotelPackages/5
        [ResponseType(typeof(PartnerHotelPackages))]
        public async Task<IHttpActionResult> DeletePartnerHotelPackages(int id)
        {
            PartnerHotelPackages partnerHotelPackages = await db.PartnerHotelPackages.FindAsync(id);
            if (partnerHotelPackages == null)
            {
                return NotFound();
            }

            db.PartnerHotelPackages.Remove(partnerHotelPackages);
            await db.SaveChangesAsync();

            return Ok(partnerHotelPackages);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PartnerHotelPackagesExists(int id)
        {
            return db.PartnerHotelPackages.Count(e => e.Id == id) > 0;
        }
    }
}