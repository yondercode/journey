﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using api.Models;
using System.Web.Http.Cors;

namespace api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PartnerHotelsController : ApiController
    {
        private DatabaseEntities1 db = new DatabaseEntities1();

        // GET: api/PartnerHotels
        public IQueryable<PartnerHotels> GetPartnerHotels()
        {
            return db.PartnerHotels;
        }

        // GET: api/PartnerHotels/5
        [ResponseType(typeof(PartnerHotels))]
        public async Task<IHttpActionResult> GetPartnerHotels(int id)
        {
            PartnerHotels partnerHotels = await db.PartnerHotels.FindAsync(id);
            if (partnerHotels == null)
            {
                return NotFound();
            }

            return Ok(partnerHotels);
        }

        // PUT: api/PartnerHotels/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPartnerHotels(int id, PartnerHotels partnerHotels)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != partnerHotels.Id)
            {
                return BadRequest();
            }

            db.Entry(partnerHotels).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PartnerHotelsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PartnerHotels
        [ResponseType(typeof(PartnerHotels))]
        public async Task<IHttpActionResult> PostPartnerHotels(PartnerHotels partnerHotels)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PartnerHotels.Add(partnerHotels);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PartnerHotelsExists(partnerHotels.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = partnerHotels.Id }, partnerHotels);
        }

        // DELETE: api/PartnerHotels/5
        [ResponseType(typeof(PartnerHotels))]
        public async Task<IHttpActionResult> DeletePartnerHotels(int id)
        {
            PartnerHotels partnerHotels = await db.PartnerHotels.FindAsync(id);
            if (partnerHotels == null)
            {
                return NotFound();
            }

            db.PartnerHotels.Remove(partnerHotels);
            await db.SaveChangesAsync();

            return Ok(partnerHotels);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PartnerHotelsExists(int id)
        {
            return db.PartnerHotels.Count(e => e.Id == id) > 0;
        }
    }
}