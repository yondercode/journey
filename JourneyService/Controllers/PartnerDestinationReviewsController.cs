﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using api.Models;

namespace api.Controllers
{
    public class PartnerDestinationReviewsController : ApiController
    {
        private DatabaseEntities1 db = new DatabaseEntities1();

        // GET: api/PartnerDestinationReviews
        public IQueryable<PartnerDestinationReviews> GetPartnerDestinationReviews()
        {
            return db.PartnerDestinationReviews;
        }

        // GET: api/PartnerDestinationReviews/5
        [ResponseType(typeof(PartnerDestinationReviews))]
        public async Task<IHttpActionResult> GetPartnerDestinationReviews(int id)
        {
            PartnerDestinationReviews partnerDestinationReviews = await db.PartnerDestinationReviews.FindAsync(id);
            if (partnerDestinationReviews == null)
            {
                return NotFound();
            }

            return Ok(partnerDestinationReviews);
        }

        // PUT: api/PartnerDestinationReviews/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPartnerDestinationReviews(int id, PartnerDestinationReviews partnerDestinationReviews)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != partnerDestinationReviews.Id)
            {
                return BadRequest();
            }

            db.Entry(partnerDestinationReviews).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PartnerDestinationReviewsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PartnerDestinationReviews
        [ResponseType(typeof(PartnerDestinationReviews))]
        public async Task<IHttpActionResult> PostPartnerDestinationReviews(PartnerDestinationReviews partnerDestinationReviews)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PartnerDestinationReviews.Add(partnerDestinationReviews);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PartnerDestinationReviewsExists(partnerDestinationReviews.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = partnerDestinationReviews.Id }, partnerDestinationReviews);
        }

        // DELETE: api/PartnerDestinationReviews/5
        [ResponseType(typeof(PartnerDestinationReviews))]
        public async Task<IHttpActionResult> DeletePartnerDestinationReviews(int id)
        {
            PartnerDestinationReviews partnerDestinationReviews = await db.PartnerDestinationReviews.FindAsync(id);
            if (partnerDestinationReviews == null)
            {
                return NotFound();
            }

            db.PartnerDestinationReviews.Remove(partnerDestinationReviews);
            await db.SaveChangesAsync();

            return Ok(partnerDestinationReviews);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PartnerDestinationReviewsExists(int id)
        {
            return db.PartnerDestinationReviews.Count(e => e.Id == id) > 0;
        }
    }
}