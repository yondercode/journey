﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using api.Models;

namespace api.Controllers
{
    public class PartnerDestinationImagesController : ApiController
    {
        private DatabaseEntities1 db = new DatabaseEntities1();

        // GET: api/PartnerDestinationImages
        public IQueryable<PartnerDestinationImages> GetPartnerDestinationImages()
        {
            return db.PartnerDestinationImages;
        }

        // GET: api/PartnerDestinationImages/5
        [ResponseType(typeof(PartnerDestinationImages))]
        public async Task<IHttpActionResult> GetPartnerDestinationImages(int id)
        {
            PartnerDestinationImages partnerDestinationImages = await db.PartnerDestinationImages.FindAsync(id);
            if (partnerDestinationImages == null)
            {
                return NotFound();
            }

            return Ok(partnerDestinationImages);
        }

        // PUT: api/PartnerDestinationImages/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPartnerDestinationImages(int id, PartnerDestinationImages partnerDestinationImages)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != partnerDestinationImages.Id)
            {
                return BadRequest();
            }

            db.Entry(partnerDestinationImages).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PartnerDestinationImagesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PartnerDestinationImages
        [ResponseType(typeof(PartnerDestinationImages))]
        public async Task<IHttpActionResult> PostPartnerDestinationImages(PartnerDestinationImages partnerDestinationImages)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PartnerDestinationImages.Add(partnerDestinationImages);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PartnerDestinationImagesExists(partnerDestinationImages.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = partnerDestinationImages.Id }, partnerDestinationImages);
        }

        // DELETE: api/PartnerDestinationImages/5
        [ResponseType(typeof(PartnerDestinationImages))]
        public async Task<IHttpActionResult> DeletePartnerDestinationImages(int id)
        {
            PartnerDestinationImages partnerDestinationImages = await db.PartnerDestinationImages.FindAsync(id);
            if (partnerDestinationImages == null)
            {
                return NotFound();
            }

            db.PartnerDestinationImages.Remove(partnerDestinationImages);
            await db.SaveChangesAsync();

            return Ok(partnerDestinationImages);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PartnerDestinationImagesExists(int id)
        {
            return db.PartnerDestinationImages.Count(e => e.Id == id) > 0;
        }
    }
}