﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using api.Models;
using System.Web.Http.Cors;

namespace api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class JourneysController : ApiController
    {
        private DatabaseEntities1 db = new DatabaseEntities1();

        // GET: api/Journeys
        public IQueryable<Journey> GetJourney()
        {
            return db.Journey;
        }

        // GET: api/Journeys/5
        [ResponseType(typeof(Journey))]
        public async Task<IHttpActionResult> GetJourney(int id)
        {
            Journey journey = await db.Journey.FindAsync(id);
            if (journey == null)
            {
                return NotFound();
            }

            return Ok(journey);
        }

        // PUT: api/Journeys/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutJourney(int id, Journey journey)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != journey.Id)
            {
                return BadRequest();
            }

            db.Entry(journey).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JourneyExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Journeys
        [ResponseType(typeof(Journey))]
        public async Task<IHttpActionResult> PostJourney(Journey journey)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Journey.Add(journey);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (JourneyExists(journey.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return Ok();
        }

        // DELETE: api/Journeys/5
        [ResponseType(typeof(Journey))]
        public async Task<IHttpActionResult> DeleteJourney(int id)
        {
            Journey journey = await db.Journey.FindAsync(id);
            if (journey == null)
            {
                return NotFound();
            }

            db.Journey.Remove(journey);
            await db.SaveChangesAsync();

            return Ok(journey);
        }

        // GET: api/Journeys/GetForUser/{id}
        public IQueryable<Journey> GetForUser(int id)
        {
            return db.Users.Find(id).Journey.AsQueryable();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool JourneyExists(int id)
        {
            return db.Journey.Count(e => e.Id == id) > 0;
        }
    }
}