﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using api.Models;

namespace api.Controllers
{
    public class PartnerHotelReviewsController : ApiController
    {
        private DatabaseEntities1 db = new DatabaseEntities1();

        // GET: api/PartnerHotelReviews
        public IQueryable<PartnerHotelReviews> GetPartnerHotelReviews()
        {
            return db.PartnerHotelReviews;
        }

        // GET: api/PartnerHotelReviews/5
        [ResponseType(typeof(PartnerHotelReviews))]
        public async Task<IHttpActionResult> GetPartnerHotelReviews(int id)
        {
            PartnerHotelReviews partnerHotelReviews = await db.PartnerHotelReviews.FindAsync(id);
            if (partnerHotelReviews == null)
            {
                return NotFound();
            }

            return Ok(partnerHotelReviews);
        }

        // PUT: api/PartnerHotelReviews/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPartnerHotelReviews(int id, PartnerHotelReviews partnerHotelReviews)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != partnerHotelReviews.Id)
            {
                return BadRequest();
            }

            db.Entry(partnerHotelReviews).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PartnerHotelReviewsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PartnerHotelReviews
        [ResponseType(typeof(PartnerHotelReviews))]
        public async Task<IHttpActionResult> PostPartnerHotelReviews(PartnerHotelReviews partnerHotelReviews)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PartnerHotelReviews.Add(partnerHotelReviews);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (PartnerHotelReviewsExists(partnerHotelReviews.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = partnerHotelReviews.Id }, partnerHotelReviews);
        }

        // DELETE: api/PartnerHotelReviews/5
        [ResponseType(typeof(PartnerHotelReviews))]
        public async Task<IHttpActionResult> DeletePartnerHotelReviews(int id)
        {
            PartnerHotelReviews partnerHotelReviews = await db.PartnerHotelReviews.FindAsync(id);
            if (partnerHotelReviews == null)
            {
                return NotFound();
            }

            db.PartnerHotelReviews.Remove(partnerHotelReviews);
            await db.SaveChangesAsync();

            return Ok(partnerHotelReviews);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PartnerHotelReviewsExists(int id)
        {
            return db.PartnerHotelReviews.Count(e => e.Id == id) > 0;
        }
    }
}