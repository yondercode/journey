﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="api.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="Content/bootstrap.min.css" rel="stylesheet">
    <link href="Content/login.css" rel="stylesheet">
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/jquery-1.10.2.min.js"></script>
</head>
<body>
    <div runat="server">
        <%--Username<p>
            <asp:TextBox ID="UsernameField" runat="server" OnTextChanged="UsernameField_TextChanged"></asp:TextBox>
        </p>
        Password<p>
            <asp:TextBox ID="PasswordField" runat="server"></asp:TextBox>
        </p>

        <p>
            <asp:Button ID="LoginButton" runat="server" OnClick="LoginButton_Click" Text="Login" />
        </p>--%>

        <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-login">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-6">
                                <a href="#" class="active" id="login-form-link">Login</a>
                            </div>
                            <div class="col-xs-6">
                                <a href="Register.aspx" id="register-form-link">Register</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <form id="loginform" runat="server" style="display: block;">
                                    <div class="form-group">
                                        <asp:TextBox ID="UsernameField" runat="server" OnTextChanged="UsernameField_TextChanged" CssClass="form-control" placeholder="Email"></asp:TextBox>
                                    </div>
                                    <div class="form-group">
                                        <asp:TextBox ID="PasswordField" runat="server" CssClass="form-control" placeholder="Password" TextMode="Password" Required></asp:TextBox>
                                    </div>
                                    <%--<div class="form-group text-center">
                                        <asp:CheckBox ID="chkRememberMe" Text="Remember Me" runat="server" />
                                    </div>--%>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3">
                                                <asp:Button ID="LoginButton" runat="server" OnClick="LoginButton_Click" Text="Login" Class="form-control btn btn-login"/>
                                            </div>
                                        </div>
                                    </div>
                                    <%--<div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="text-center">
                                                    <a href="https://phpoll.com/recover" tabindex="5" class="forgot-password">Forgot Password?</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>--%>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>

    

</body>
</html>
