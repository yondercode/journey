﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using api.Models;

namespace api
{
    public partial class Register : System.Web.UI.Page
    {

        private DatabaseEntities1 db = new DatabaseEntities1();

        protected void Page_Load(object sender, EventArgs e)
        {
            // Display the errors if available
            List<string> errors = Session["RegistrationErrors"] as List<string>;
            if (errors != null && errors.Count() > 0)
            {
                string errorString = "";
                foreach (string error in errors)
                {
                    errorString = errorString + (errorString == "" ? "" : "/n") + error;
                }
                ValidationErrorDisplay.Text = errorString;
                errors.Clear();
            }
        }

        protected void RegisterButton_Click(object sender, EventArgs e)
        {
            // Get the field values
            string email = EmailField.Text;
            string password = PasswordField.Text;
            string passwordConfirmation = PasswordConfirmationField.Text;
            string displayName = DisplayNameField.Text;

            List<string> errors = new List<string>();

            // Validate the forms
            if (email == "")
            {
                errors.Add("Email tidak boleh kosong!");
            }
            else if (password == "")
            {
                errors.Add("Password tidak boleh kosong!");
            }
            else if (password != passwordConfirmation)
            {
                errors.Add("Konfirmasi password tidak sama!");
            }
            else if (db.Users.Where(user => user.Email == email).Count() > 0)
            {
                errors.Add("Sudah ada akun dengan email ini!");
            }

            if (errors.Count() > 0)
            {
                Session["RegistrationErrors"] = errors;
                Response.Redirect(Request.RawUrl);
                return;
            }

            if (displayName == "")
            {
                displayName = email;
            }

            // Create a new record in the user database and redirects the user
            Users newUser = new Users
            {
                Email = email,
                PasswordHash = password.GetHashCode().ToString("x"),
                DisplayName = displayName
            };

            db.Users.Add(newUser);
            db.SaveChanges();

            Response.Redirect("~/");
        }
    }
}