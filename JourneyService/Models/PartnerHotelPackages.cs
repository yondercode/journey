//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace api.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PartnerHotelPackages
    {
        public int Id { get; set; }
        public int PartnerHotelId { get; set; }
        public string name { get; set; }
        public decimal price { get; set; }
        public string PreviewImageUrl { get; set; }
    
        public virtual PartnerHotels PartnerHotels { get; set; }
    }
}
