﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using api.Models;

namespace api
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private DatabaseEntities1 db = new DatabaseEntities1();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(sender);
            System.Diagnostics.Debug.WriteLine(e.ToString());
        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            string username = UsernameField.Text.ToString();
            string password = PasswordField.Text.ToString().GetHashCode().ToString("x");

            var result = db.Users.Where(user => user.Email == username && user.PasswordHash == password);

            // If there is a matching credentials, generate an auth token and redirect the user to the main page
            if (result.Count() > 0)
            {
                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
                var stringChars = new char[64];
                var random = new Random();

                for (int i = 0; i < stringChars.Length; i++)
                {
                    stringChars[i] = chars[random.Next(chars.Length)];
                }

                string authToken = new String(stringChars);
                result.First().AuthToken = authToken;
                db.Entry(result.First()).State = EntityState.Modified;
                db.SaveChanges();

                Session["AuthToken"] = "\"" + authToken + "\"";

                Response.Redirect("~/");
            }
            else
            {
                Response.Redirect(Request.RawUrl);
            }
        }

        protected void UsernameField_TextChanged(object sender, EventArgs e)
        {

        }
    }
}